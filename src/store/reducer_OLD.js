import * as actionTypes from './actions/actionsTypes';

const initialState = {
    counter: 0,
    results: []
};

// OLD REDUCER FILE BEFORE SPLITTING TO TWO SEPARATED REDUCERS - THIS FILE IS NOT USED ANYMORE

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INCREMENT:
            // WHAT NOT TO DO!!! BECAUSE WE MUTATE THE OLD STATE
            /*const newState = state;
            newState.counter = state.counter + 1;
            return newState;*/

            const newState = Object.assign({}, state);  // we create deep copy, not the clone, so results object is still the same
            newState.counter = state.counter + 1;
            return newState;                            // or we can use the '...state' syntax to do that
        case actionTypes.DECREMENT:
            return {
                ...state,
                counter: state.counter - 1
            };
        case actionTypes.ADD:
            return {
                ...state,
                counter: state.counter + action.val
            };
        case actionTypes.SUBTRACT:
            return {
                ...state,
                counter: state.counter - action.val
            };
        case actionTypes.STORE_RESULT:
            return {
                ...state,
                results: state.results.concat({value: state.counter, id: new Date()})    // should use concat, not push
            };
        case actionTypes.DELETE_RESULT:
            //const id = 2;
            //const newArray = [...state.results];    // array items are still pointing to the same object as from original state - if we want to modify object inside array, we have to copy them too
            //newArray.splice(id, 1);

            //const updatedArray = state.results.filter((result, index) => index !== id);
            const updatedArray = state.results.filter(result => result.id !== action.resultElId);

            return {
                ...state,
                results: updatedArray
            }
    }

    return state;
};

export default reducer;
