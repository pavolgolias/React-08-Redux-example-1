import * as actionTypes from '../actions/actionsTypes';
import {updateObject} from '../utility';

const initialState = {
    results: []
};

const deleteResult = (state, action) => {
    const updatedArray = state.results.filter(result => result.id !== action.resultElId);
    return updateObject(state, {results: updatedArray})
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STORE_RESULT:
            /*return {
                ...state,
                results: state.results.concat({value: action.result * 2, id: new Date()})    // should use concat, not push
            };*/
            return updateObject(state, {results: state.results.concat({value: action.result * 2, id: new Date()})});
        case actionTypes.DELETE_RESULT:
            //const id = 2;
            //const newArray = [...state.results];    // array items are still pointing to the same object as from original state - if we want to modify object inside array, we have to copy them too
            //newArray.splice(id, 1);

            //const updatedArray = state.results.filter((result, index) => index !== id);
            return deleteResult(state, action);
    }

    return state;
};

export default reducer;
