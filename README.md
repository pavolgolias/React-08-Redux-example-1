## JS-08 - React example project - Redux
This is an example React project covering multiple React features. Based on UDEMY course: https://www.udemy.com/react-the-complete-guide-incl-redux/.

**Examples in this project are focused mainly on Redux.**

**See all branches to see all features.**

#### Used commands and libs:
* create-react-app my-app-name
* npm install
* npm start
* npm install --save radium
* npm run eject
* npm install --save prop-types
* npm install axios --save
* npm install --save react-router react-router-dom
* npm install --save redux
* npm install --save react-redux
* npm install --save redux-thunk
* node redux-basics.js (for example of usage redux, because redux is completely separated and independent lib)

