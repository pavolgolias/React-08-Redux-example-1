import * as actionTypes from '../actions/actionsTypes';
import {updateObject} from '../utility';

const initialState = {
    counter: 0
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.INCREMENT:
            // WHAT NOT TO DO!!! BECAUSE WE MUTATE THE OLD STATE
            /*const newState = state;
            newState.counter = state.counter + 1;
            return newState;*/

            /*const newState = Object.assign({}, state);  // we create deep copy, not the clone, so results object is still the same
            newState.counter = state.counter + 1;
            return newState;*/                            // or we can use the '...state' syntax to do that
            return updateObject(state, {counter: state.counter + 1});
        case actionTypes.DECREMENT:
            return updateObject(state, {counter: state.counter - 1});
            /*return {
                ...state,
                counter: state.counter - 1
            };*/
        case actionTypes.ADD:
            return updateObject(state, {counter: state.counter + action.val});
        case actionTypes.SUBTRACT:
            return updateObject(state, {counter: state.counter - action.val});
    }

    return state;
};

export default reducer;
