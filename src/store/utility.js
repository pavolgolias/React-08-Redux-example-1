export const updateObject = (oldUpdate, updatedValues) => {
    return {
        ...oldUpdate,
        ...updatedValues
    }
};
